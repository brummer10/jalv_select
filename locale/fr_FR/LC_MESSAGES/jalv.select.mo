��          �   %   �      0     1     A     R     a     q     w     ~     �     �     �  	   �     �     �     �  %   �  *        D     J     P  #   Y     }     �     �  F  �          /     A     S     d     k     s     �     �     �     �     �  +   �       5     7   L     �     �     �  #   �     �  0   �  +                               
                                        	                                                           
Audio Inputs:  
Audio Outputs:  
Midi Inputs:  
Midi Outputs:   
by   _All   invalid plugins found  valid plugins installed
 Default Favorite LV2 plugs Name Preset <%s> has no rdfs:label
 Quit X Error:  %s
 Global HotKey disabled
 X Error: try ControlMask | ShiftMask now 
 _Fav. _Quit _Refresh jalv.select * Unknown Message
 %2s
 print version string and exit start minimized in systray start with given high in pixel Project-Id-Version: jalv.select 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-03-27 22:18+0200
Last-Translator: Olivier Humbert <trebmuh@tuxfamily.org>
Language-Team: French <>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
Entrées audio :  
Sorties audio :  
Entrées MIDI :  
Sorties MIDI :   
par   _Tous   greffons invalides installés  greffons valides installés
 Défaut Favoris Greffons LV2 Greffons disponibles Le préréglage <%s> n&apos; de rdfs:label
 Quitter Erreur X :  %s
 Raccourci-clavier global désactivé
 Erreur X : essayez ControlMask | ShiftMask maintenant 
 _Fav. _Quitter _Rafraîchir jalv.select * Message inconnu
 %2s
 affiche la version et quitte démarrer minimisé dans la zone de notification démarrer avec la hauteur donnée en pixels 